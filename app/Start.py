import sys
import os
import apt
import time
import pkg_resources
import netifaces
import ipaddress
import pprint

sys.path.append('../')
from datetime import datetime
from app.Tests.PortSecurityTest import PortSecurityTest
from app.Tests.ArpInspectionTest import ArpInspectionTest
from app.Tests.DhcpSnoopingTest import DhcpSnoopingTest
from app.Tests.StpBpduGuardTest import StpBpduGuardTest
from app.Tests.StpRootGuardTest import StpRootGuardTest
from app.TestDependency import TestDependency
from app import Utils, globals


# main program
def main():
    # print the ASCII art application logo
    print("======================================================")
    print("            _   __                      _ _         ")
    print(" _ __   ___| |_/ _\ ___  ___ _   _ _ __(_| |_ _   _ ")
    print("| '_ \ / _ | __\ \ / _ \/ __| | | | '__| | __| | | |")
    print("| | | |  __| |__\ |  __| (__| |_| | |  | | |_| |_| |")
    print("|_| |_|\___|\__\__/\___|\___|\__,_|_|  |_|\__|\__, |")
    print("                                              |___/ ")
    print("  .--.  .-. .-.  .--.  .-. .-.  .-..---. .----..----. ")
    print(" / {} \ |  `| | / {} \ | |  \ \/ /{_   / | {_  | {}  }")
    print("/  /\  \| |\  |/  /\  \| `--.}  {  /    }| {__ | .-. |")
    print("`-'  `-'`-' `-'`-'  `-'`----'`--'  `---' `----'`-' `-'")
    print("======================================================")
    print("======================================================")
    print("======== This tool will not work correctly in  =======")
    print("== virtualized environments (Hyper-V, VMware, etc.) ==")
    print("======================================================")
    print("======================================================")

    pp = pprint.PrettyPrinter(indent=2)

    # some checks
    if os.geteuid() != 0:
        print('[ERROR] Application not runs under root!')
        sys.exit(0)

    # check if required Unix packages are installed
    __aptCache = apt.Cache()
    __installedPackaged = []

    for package in __aptCache:
        if __aptCache[package].is_installed:
            __installedPackaged.append(package.name)

    __requiredUnixPackages = [
        "python3",
        "macchanger",
        "ifupdown",
        "net-tools",
        "ethtool"
    ]
    __unixPackageInstalled = all(items in __installedPackaged for items in __requiredUnixPackages)

    if __unixPackageInstalled:
        print('[INFO] All required Unix packages are installed')
    else:
        print('[ERROR] Some required Unix packages are missing:')
        for missingPackage in (set(__requiredUnixPackages) - set(__installedPackaged)):
            print('-', missingPackage)
        sys.exit(0)

    # check if all required Python modules are installed
    __requiredPythonModules = [
        'netifaces',
        'ipaddress',
        'ping3',
        'psutil',
        'python-iptables',
        'scapy',
        'pyersinia',
        'netaddr',
        'python-dateutil'
    ]
    __pyModulesInstalled = True

    for reqModule in __requiredPythonModules:
        try:
            pkg_resources.get_distribution(reqModule)
        except:
            print('Python module "', reqModule, '" not installed.')
            __pyModulesInstalled = False

    if not __pyModulesInstalled:
        print(
            '[ERROR] Some required Python modules (see above) are not installed. Use pip3 command for install them. '
            'Exiting...')
        sys.exit(0)
    else:
        print('[INFO] All required Python modules are installed')

    # variables declaration
    # ------------------------------
    networkIfCount = len(netifaces.interfaces())
    networkIfHashMap = {}
    loopbackInterface = 'lo'

    # ask user for debug mode enable
    userInputDebugMode = ''
    while not userInputDebugMode in ['y', 'n']:
        userInputDebugMode = input('Do you want to enable debug mode (more verbose)? [y/n]:')

    if userInputDebugMode is 'y':
        globals.debugMode = True

    # get all network interfaces except loopback
    for i in range(0, networkIfCount):
        if not netifaces.interfaces()[i] == loopbackInterface:
            networkIfHashMap[i] = netifaces.interfaces()[i]

    # ask user for select the primary interface (in the diploma thesis sometimes called as verification interface)
    primaryInterfaceNum = '-1'
    while not primaryInterfaceNum.isdigit() or not int(primaryInterfaceNum) in range(0, networkIfCount) or str(
            networkIfHashMap.get(int(primaryInterfaceNum))) is loopbackInterface:
        primaryInterfaceNum = input(
            "[PROMPT] Enter number of primary network interface " + pp.pformat(networkIfHashMap) + ":")

    # ask user for select the attacking interface
    attackingInterfaceNum = '-1'
    while not attackingInterfaceNum.isdigit() or not int(attackingInterfaceNum) in range(0,
                                                                                         networkIfCount) or attackingInterfaceNum is primaryInterfaceNum or str(
        networkIfHashMap.get(int(attackingInterfaceNum))) is loopbackInterface:
        attackingInterfaceNum = input(
            "[PROMPT] Enter number of network interface for attacks " + pp.pformat(networkIfHashMap) + ':')

    __primaryInterface = networkIfHashMap.get(int(primaryInterfaceNum))
    __attackingInterface = networkIfHashMap.get(int(attackingInterfaceNum))

    # Get current interface IP and gateway and check them on interfaces
    if not Utils.isNetworkIfUp(__primaryInterface):
        print("[ERROR] Network interface is down. Exiting...")
    elif (not Utils.hasNetworkIfIpAddr(__primaryInterface)) or (not Utils.hasNetworkIfIpAddr(__attackingInterface)):
        print('[ERROR] Both network interfaces have to have an IP address. Exiting...')
    elif not netifaces.gateways()['default'][netifaces.AF_INET][0]:
        print('[ERROR] No default gateway found. Exiting...')
    else:
        __primaryInterfaceIpStr = netifaces.ifaddresses(__primaryInterface)[netifaces.AF_INET][0]['addr']
        __attackingInterfaceIpStr = netifaces.ifaddresses(__attackingInterface)[netifaces.AF_INET][0]['addr']
        __gatewayIpStr = netifaces.gateways()[netifaces.AF_INET][0][0]

        __primaryInterfaceIp = ipaddress.ip_address(__primaryInterfaceIpStr)
        __attackingInterfaceIp = ipaddress.ip_address(__attackingInterfaceIpStr)
        __gatewayIp = netifaces.gateways()['default'][netifaces.AF_INET][0]

        if globals.debugMode:
            print('[DEBUG] IP address (', __primaryInterface, '): ', __primaryInterfaceIp, sep='')
            print('[DEBUG] IP address (', __attackingInterface, '): ', __attackingInterfaceIp, sep='')
            print('[DEBUG] Gateway IP: ', __gatewayIp, sep='')

        # check if STP protocol is enabled in network
        __stpEnabled = False

        if Utils.getSniffedStpPacket(__primaryInterface):
            __stpEnabled = True
        else:
            pass
            print('[WARN] STP protocol not enabled in this network. STP tests will not be available!')

        # create the security test instances
        __portSecurityTest = PortSecurityTest()
        __arpInspectionTest = ArpInspectionTest()
        __dhcpSnoopingTest = DhcpSnoopingTest()
        __stpBpduGuardTest = StpBpduGuardTest()
        __stpRootGuardTest = StpRootGuardTest()

        __testsToRun = {}
        testResults = {}

        # reduce the test menu according STP presence in network
        if __stpEnabled:
            __allTests = {
                0: __dhcpSnoopingTest,
                1: __arpInspectionTest,
                2: __portSecurityTest,
                3: __stpBpduGuardTest,
                4: __stpRootGuardTest
            }
        else:
            __allTests = {
                0: __dhcpSnoopingTest,
                1: __arpInspectionTest,
                2: __portSecurityTest
            }

        # some test start depends on passing another test...
        testDependencies = [TestDependency(__dhcpSnoopingTest, __arpInspectionTest, True),
                            TestDependency(__stpBpduGuardTest, __stpRootGuardTest, False)]

        # ask user for run all test consecutively
        userInputAllTest = ''
        while not userInputAllTest in ['y', 'n']:
            userInputAllTest = input('Do you want to run all tests simultaneously? [y/n]:')

        if userInputAllTest is 'y':
            __testsToRun = __allTests.copy()
        else:
            # otherwise user can select the required test
            selectedTestNumber = '-1'
            while not selectedTestNumber.isdigit() or not int(selectedTestNumber) in range(0, len(__allTests)):
                selectedTestNumber = input(
                    'Enter number of the test you want to run' + Utils.getTestNamesFromDict(__allTests) + ':')
            __testsToRun = {0: __allTests.get(int(selectedTestNumber))}

        # iterate over all tests which will be started
        for testId, test in __testsToRun.items():
            print('Current test:', test.getTestName())
            if Utils.checkReadinessForTest(__primaryInterface, __attackingInterface):

                # check if dependency exists for current test
                testDependency = next(
                    (dependency for dependency in testDependencies if dependency.slaveTest == test), None)
                # if dependency check not passed then skip the test
                if (not testDependency is None and userInputAllTest is 'y') and (not testResults.get(testDependency.masterTest.getTestName()) is testDependency.masterTestResult):
                    print(test.getTestName(), ' will be skipped because the ',
                          testDependency.masterTest.getTestName(), "'s result was not ",
                          testDependency.masterTestResult, sep='')
                    input('[PROMPT] Press ENTER to continue...')
                else:
                    # print the test name, test manual and start them after user confirmation
                    Utils.clearSystemConsole()
                    print('>>>>>>>>>', test.getTestName(), '<<<<<<<<<')
                    print(test.getTestManual())
                    input('\n[PROMPT] Press ENTER to start the ' + test.getTestName() + '...')
                    print(test.getTestName(), 'starting...')

                    # get current timestamp
                    testStartTime = datetime.now().strftime("%H:%M:%S")

                    test.start(__primaryInterface, __attackingInterface)
                    print(test.getTestName(), "completed. Result:", test.testPassed())

                    # stop rest dhclient processes created during the test
                    Utils.stopProcessesFromTime(['dhclient'], testStartTime)
                    Utils.killProcessesFromTime(['dhclient'], testStartTime)

                    # store the test result
                    testResults[test.getTestName()] = test.testPassed()

                    # reset network interfaces
                    Utils.resetNetworkIf(__primaryInterface)
                    Utils.resetNetworkIf(__attackingInterface)
                    time.sleep(4)
            else:
                print('Preparation checks failed. Exiting...')
                sys.exit(0)

        # show the final report of all performed security tests
        print("=======================================")
        print("===== netSecurity Analyser REPORT =====")
        print("=======================================")

        for name, result in testResults.items():
            if result:
                print('[PASSED]', name, 'passed. This protection is enabled.')
            else:
                print('[FAILED]', name, 'failed. This protection is not enabled.')


###################################################
if __name__ == '__main__':
    main()
