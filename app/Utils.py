import subprocess
import os
import netifaces
import psutil
import signal
import time
from scapy.all import sniff
from app import globals


def __runSystemCommand(commandString):
    if globals.debugMode:
        print('[DEBUG] Running command:"' + commandString + '"')

    os.system(commandString + " > /dev/null ")


def __getCommandOutput(commandString):
    if globals.debugMode:
        print('[DEBUG] Running command:"' + commandString + '"')

    return subprocess.check_output(commandString, shell=True)


def bringNetworkIfDown(networkIf):
    __runSystemCommand("ip link set dev " + networkIf + " down")


def bringNetworkIfUp(networkIf):
    __runSystemCommand("ip link set dev " + networkIf + " up")


def falseNetworkIfMacAddr(networkIf):
    __runSystemCommand("macchanger -e " + networkIf + " > /dev/null")

    if globals.debugMode:
        print('[DEBUG] MAC addr on', networkIf, 'falsed')


def revertNetworkIfMacAddr(networkIf):
    __runSystemCommand("macchanger -p " + networkIf + " > /dev/null")

    if globals.debugMode:
        print('[DEBUG] MAC addr on', networkIf, 'reverted back to the original')


def flushArpCache():
    __runSystemCommand("ip neighbour flush all")


def isIpInArpCache(networkIf, ip):
    strOutput = __getCommandOutput("arp -n -i " + networkIf + " | awk '/" + str(ip) + "/{print $3}'")

    if not strOutput:
        return False
    else:
        return True


def isIpReachableFromNetworkIf(ip, networkIf, timeout):
    try:
        strOutput = __getCommandOutput("ping -c 1 -w " + str(timeout) + " -I " + str(networkIf) + " " + str(ip))

        if "1 received" in str(strOutput):
            return True
        else:
            return False
    except:
        return False


def startIcmpTrafficSimulation(srcIf, dstIp):
    __runSystemCommand("ping -I " + str(srcIf) + " " + str(dstIp))


def bindIpToNetIf(networkIf, ip):
    __runSystemCommand("ip addr add " + str(ip) + " dev " + networkIf + " 2> /dev/null")


def unbindIpFromNetIf(networkIf, ip):
    __runSystemCommand("ip addr del " + str(ip) + " dev " + networkIf + " 2> /dev/null")


def unbindAllIpsFromNetIf(networkIf):
    __runSystemCommand("ip addr flush dev " + networkIf)


def renewIpFromDhcp(networkIf):
    __runSystemCommand("dhclient " + networkIf + " 2> /dev/null")


def releaseIpFromDhcp(networkIf):
    __runSystemCommand("dhclient -r " + networkIf + " 2> /dev/null")


def obtainNewIpFromDhcp(networkIf):
    releaseIpFromDhcp(networkIf)
    renewIpFromDhcp(networkIf)


def isNetworkIfUp(networkIf):
    strOutput = str(__getCommandOutput("cat /sys/class/net/" + networkIf + "/operstate"))

    if "up" in strOutput:
        return True
    elif "down" in strOutput:
        return False


def hasNetworkIfIpAddr(networkIf):
    try:
        ip = netifaces.ifaddresses(str(networkIf))[netifaces.AF_INET][0]['addr']

        if ip:
            return True
        else:
            return False
    except:
        return False


def fwDropDhcpOffers(networkIf):
    pass


def createVirtualNetworkIf(name, parentNetworkIf, macAddr, type):
    networkIfTypes = ['vlan', 'veth', 'vcan', 'vxcan', 'dummy', 'ifb', 'macvlan', 'macvtap', 'bridge', 'bond', 'team',
                      'ipoib', 'ip6tnl', 'ipip', 'sit', 'vxlan', 'gre', 'gretap', 'erspan', 'ip6gre', 'ip6gretap',
                      'ip6erspan', 'vti', 'nlmon', 'team_slave', 'bond_slave', 'ipvlan', 'geneve', 'bridge_slave',
                      'vrf', 'macsec']

    if parentNetworkIf in netifaces.interfaces() and type in networkIfTypes:
        __runSystemCommand(
            'ip link add link ' + parentNetworkIf + ' name ' + name + ' address ' + macAddr + ' type ' + type)
        bringNetworkIfUp(name)
    else:
        print('[ERROR] New virtual interface', name, 'cannot be created')


def removeVirtualNetworkIf(name):
    __runSystemCommand("ip link delete " + name)


def resetNetworkIf(networkIf):
    if globals.debugMode:
        print('[DEBUG] Resetting network interface', networkIf)

    # revert back the original MAC addr
    unbindAllIpsFromNetIf(networkIf)

    bringNetworkIfDown(networkIf)
    revertNetworkIfMacAddr(networkIf)
    bringNetworkIfUp(networkIf)

    time.sleep(3)

    # obtain the IP addr
    if not hasNetworkIfIpAddr(networkIf):
        obtainNewIpFromDhcp(networkIf)


def checkReadinessForTest(__primaryInterface, __attackingInterface):
    checks = [isNetworkIfUp(__primaryInterface), isNetworkIfUp(__attackingInterface),
              hasNetworkIfIpAddr(__primaryInterface), hasNetworkIfIpAddr(__attackingInterface)]

    if checks.__contains__(False):
        if globals.debugMode:
            print('[DEBUG] Preparation checks failed. Check network interfaces')

        return False
    else:
        if globals.debugMode:
            print('[DEBUG] Preparation checks passed')

        return True


def getTestNamesFromDict(dict):
    outputString = ''

    for key, value in dict.items():
        outputString += ('\n' + str(key) + ' = ' + value.getTestName())

    return outputString + '\n'


def echoOff():
    pass


def clearSystemConsole():
    print(chr(27) + '[2j')
    print('\033c')
    print('\x1bc')


def stopProcessesFromTime(processesNamesArray, fromTimeStamp):
    for p in psutil.process_iter():
        if p.name() in processesNamesArray and time.strftime("%H:%M:%S",
                                                             time.localtime(p.create_time())) > fromTimeStamp:
            if globals.debugMode:
                print('[DEBUG] Try to stop process', p.name())

            os.kill(p.pid, signal.SIGSTOP)


def killProcessesFromTime(processesNamesArray, fromTimeStamp):
    for p in psutil.process_iter():
        if p.name() in processesNamesArray and time.strftime("%H:%M:%S",
                                                             time.localtime(p.create_time())) > fromTimeStamp:
            if globals.debugMode:
                print('[DEBUG] Try to stop process', p.name())

            os.kill(p.pid, signal.SIGKILL)


def getSniffedStpPacket(networkIf):
    stpPacket = sniff(filter="stp", count=1, iface=networkIf, timeout=5)

    if len(stpPacket) > 0:
        return list(stpPacket)[0]
    else:
        return None
