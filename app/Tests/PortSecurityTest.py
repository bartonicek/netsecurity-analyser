import time
import ipaddress
import netifaces

from app import Utils, globals
from randmac import randmac
from datetime import datetime
from scapy.layers.l2 import Ether
from multiprocessing import Process
from scapy.all import RandNum, sendp


class PortSecurityTest:
    # base variables declaration
    __name = 'Port Security test'
    __testManual = 'Please connect both network interfaces to general access ports for endpoint devices (e.g. computer).' \
                   'DHCP server presence required. Make the switch CAM table empty is recommended.'
    __partialResults = []

    def __init__(self):
        pass

    def start(self, networkIf, attackingIf):
        try:
            gatewayIp = netifaces.gateways()['default'][netifaces.AF_INET][0]
        except:
            print('[ERROR] Error during determine the gateway IP')

        testPingIp = gatewayIp
        pingTimeout = 1
        # Define the number of virtual devices
        maxNumberOfSimulatedDevices = 4

        # deactivate main interface (because of verification relevancy)
        Utils.bringNetworkIfDown(networkIf)

        # get current timestamp
        startTime = datetime.now().strftime("%H:%M:%S")

        # create separated process for each virtual interface
        testPingProceeses = []
        virtualInterfaces = []

        time.sleep(2)

        for i in range(0, maxNumberOfSimulatedDevices):
            # create the new virtual L2 network interface with random MAC address
            vInterfaceName = 'veth' + str(i)
            randomMacAddr = str(randmac.RandMac('00:00:00:00:00:00', True))
            Utils.createVirtualNetworkIf(vInterfaceName, attackingIf, randomMacAddr, 'macvlan')
            virtualInterfaces.append(vInterfaceName)
            time.sleep(3)

            # detect port 'down' state in case of 'shutdown' violation action in port-security configuration
            if not Utils.isNetworkIfUp(attackingIf):
                self.__partialResults.append(False)
                break

            # try to get IP from DHCP server
            Utils.renewIpFromDhcp(vInterfaceName)
            time.sleep(3)

            # otherwise choose an available IP from the same network
            if not Utils.hasNetworkIfIpAddr(vInterfaceName):
                atttackingIfIp = netifaces.ifaddresses(attackingIf)[netifaces.AF_INET][0]['addr']
                netmask = netifaces.ifaddresses(attackingIf)[netifaces.AF_INET][0]['netmask']
                currentNetworkCIDR = ipaddress.ip_network(atttackingIfIp + '/' + netmask, False)
                chosenIp = ipaddress.ip_address(list(currentNetworkCIDR.hosts())[RandNum(1, 254)])

                while Utils.isIpReachableFromNetworkIf(chosenIp, attackingIf, 3) and Utils.isIpInArpCache(attackingIf,
                                                                                                          chosenIp):
                    chosenIp = ipaddress.ip_address(list(currentNetworkCIDR.hosts())[RandNum(1, 254)])

                # bind the IP to the new virtual interface manually
                Utils.bindIpToNetIf(vInterfaceName, chosenIp)

            # first, try to make one ICMP ping test...
            if self.testPingFromInterface(testPingIp, virtualInterfaces[i]):
                self.__partialResults.append(True)
                # if it passed, ICMP continuous simulation can be started in separated process
                testPingProceeses.append(Process(target=Utils.startIcmpTrafficSimulation, name="Test ping process",
                                                 args=(vInterfaceName, testPingIp)))
                testPingProceeses[-1].start()
                time.sleep(2)
            else:
                # otherwise stop the for loop
                self.__partialResults.append(False)
                break

        # cleaning process
        for i in range(0, len(testPingProceeses)):
            try:
                testPingProceeses[i].terminate()
            except:
                pass

        time.sleep(2)

        # stop ping and dhclient processes
        Utils.stopProcessesFromTime(['ping', 'dhclient', 'sh'], startTime)
        Utils.killProcessesFromTime(['ping', 'dhclient', 'sh'], startTime)

        # remove all virtual interfaces
        for vNetIf in virtualInterfaces:
            Utils.releaseIpFromDhcp(vNetIf)
            Utils.bringNetworkIfDown(vNetIf)
            Utils.removeVirtualNetworkIf(vNetIf)

        Utils.bringNetworkIfUp(networkIf)
        time.sleep(3)
        Utils.renewIpFromDhcp(networkIf)

    def testPingFromInterface(self, dstIp, srcIf):
        return Utils.isIpReachableFromNetworkIf(dstIp, srcIf, 12)

    def sendRandomBrodcast(self, networkIf, srcMac, pktCount):
        packet = Ether(src=srcMac.replace("'", ""), dst="ff:ff:ff:ff:ff:ff")
        sendp(packet, count=pktCount, iface=networkIf)

    def testPassed(self):
        if globals.debugMode:
            print('[DEBUG] Partial results: ', self.__partialResults)

        if self.__partialResults.__contains__(False):
            return True
        else:
            return False

    def getTestName(self):
        return self.__name

    def getTestManual(self):
        return self.__testManual
