import time
import sys
import netifaces
import pyersinia_lib.api

from app import Utils, globals
from multiprocessing import Process, Manager


class StpBpduGuardTest:
    # base variables declaration
    manager = Manager()
    __name = 'STP BPDU guard test'
    __testManual = 'Please connect both network interfaces to general access ports for endpoint devices (e.g. computer).' \
                   '!Warning! This test may cause the switch port to be disabled . After that network administrator must re-enable it.'
    __partialResults = manager.list()

    def __init__(self):
        pass

    def start(self, networkIf, attackingIf):
        try:
            networkIfIp = netifaces.ifaddresses(networkIf)[netifaces.AF_INET][0]['addr']
            gatewayIp = netifaces.gateways()['default'][netifaces.AF_INET][0]
        except:
            print('[ERROR] Error during determine the gateway IP')

        testPingIp = gatewayIp

        # declare some parameters for STP BPDU attack
        attackConfig = pyersinia_lib.api.GlobalParameters()
        attackConfig.attack = ['stp_conf']
        attackConfig.interface = [attackingIf]

        # define the attack and verification processes
        attackProcess = Process(target=self.startAttack, name='stpBpduAttackProcess', args=[attackConfig])
        attackVerification = Process(target=self.startAttackVerification, name='stpBpduAttackVerificationProcess',
                                     args=[attackingIf, self.__partialResults])

        # start the attack
        attackProcess.start()

        time.sleep(4)
        # start the attack verification
        attackVerification.start()
        attackVerification.join()

        attackProcess.terminate()

    def startAttack(self, attackGlobalParameters):
        sys.stderr = Utils.echoOff()

        # start the attack
        pyersinia_lib.api.run_console(attackGlobalParameters)

    def startAttackVerification(self, attackingInterface, resultList):
        # simply check if the attacking interface is still up after the attack
        if Utils.isNetworkIfUp(attackingInterface):
            resultList.append(True)
        else:
            resultList.append(False)

    def testPassed(self):
        if globals.debugMode:
            print('[DEBUG] Partial results: ', self.__partialResults)

        if self.__partialResults.__contains__(False):
            return True
        else:
            return False

    def getTestName(self):
        return self.__name

    def getTestManual(self):
        return self.__testManual
