import time
import netifaces
import ipaddress

from app import Utils, globals
from scapy.all import RandNum


class ArpInspectionTest:
    # base variables declaration
    __name = 'ARP inspection test'
    __testManual = 'Please connect both network interfaces to general access ports for endpoint devices (e.g. computer).'
    __partialResults = []

    def __init__(self):
        pass

    def start(self, networkIf, attackingIf):
        # attemptCount variable can be edited
        attemptCount = 3
        hostsCount = 10

        # get list of IP addresses from the current network
        testIp = netifaces.gateways()['default'][netifaces.AF_INET][0]

        atttackingIfIp = netifaces.ifaddresses(attackingIf)[netifaces.AF_INET][0]['addr']
        netmask = netifaces.ifaddresses(attackingIf)[netifaces.AF_INET][0]['netmask']
        currentNetwork = ipaddress.ip_network(atttackingIfIp + '/' + netmask, False)
        availableIpList = list(currentNetwork.hosts())[:hostsCount]

        Utils.bringNetworkIfDown(networkIf)

        Utils.flushArpCache()
        time.sleep(2)

        forexp = (i for i in range(0, attemptCount))

        # for each IP address in the list (and while the attemptCount limit has not been reached)
        for i in forexp:
            # choose a random IP from the list
            ip = availableIpList.__getitem__(int(RandNum(0, (len(availableIpList) - 1))))

            # convert it to CIDR format
            testedIpWithPrefix = (str(ip) + "/" + str(currentNetwork.prefixlen))

            # if the IP is not reachable in network and has not been found in the ARP cache...
            if not Utils.isIpInArpCache(attackingIf, str(ip)) and not Utils.isIpReachableFromNetworkIf(ip,
                                                                                                         attackingIf,
                                                                                                         5):

                netIfIp = ipaddress.ip_address(
                    netifaces.ifaddresses(attackingIf)[netifaces.AF_INET][0]['addr'])
                Utils.unbindIpFromNetIf(attackingIf, netIfIp)
                time.sleep(2)

                # ...then bind the IP to the attacking interface
                Utils.bindIpToNetIf(attackingIf, testedIpWithPrefix)

                time.sleep(4)

                # check if communication is possible from interface with static IP configuration
                if Utils.isIpReachableFromNetworkIf(testIp, attackingIf, 10):
                    self.__partialResults.append(True)
                else:
                    self.__partialResults.append(False)

        # Remove all static IPs and return IP back to DHCP
        Utils.flushArpCache()
        time.sleep(2)
        Utils.unbindAllIpsFromNetIf(attackingIf)
        time.sleep(2)

        Utils.bringNetworkIfUp(networkIf)

    def testPassed(self):
        if globals.debugMode:
            print('[DEBUG] Partial results: ', self.__partialResults)

        if self.__partialResults.__contains__(False):
            return True
        else:
            return False

    def getTestName(self):
        return self.__name

    def getTestManual(self):
        return self.__testManual
