import time
import netifaces
import pyersinia_lib.api

from app import Utils, globals
from multiprocessing import Process, Manager


class StpRootGuardTest:
    # base variables declaration
    manager = Manager()
    __name = 'STP Root Guard test'
    __testManual = 'Please connect both network interfaces to general access ports for endpoint devices (e.g. computer)' \
                   ' or to trunk port on instead of some slave switch (not the main switch in network).' \
                   '!Warning! This test may caused the network outage for a few mintues!'
    __partialResults = manager.list()

    def __init__(self):
        pass

    def start(self, networkIf, attackingIf):
        try:
            networkIfIp = netifaces.ifaddresses(networkIf)[netifaces.AF_INET][0]['addr']
            gatewayIp = netifaces.gateways()['default'][netifaces.AF_INET][0]
        except:
            print('[ERROR] Error during determine the gateway IP')

        testPingIp = gatewayIp

        time.sleep(2)

        # attack parameters declaration
        attackConfig = pyersinia_lib.api.GlobalParameters()
        attackConfig.attack = ['stp_root_role']
        attackConfig.interface = [attackingIf]

        verificationProcesses = []
        numberOfVerifProcesses = 3

        # create attack and verification processes
        attackProcess = Process(target=self.startAttack, name='stpRootAttackProcess', args=[attackConfig])

        for i in range(numberOfVerifProcesses):
            verificationProcesses.append(
                Process(target=self.startAttackVerification, name='stpRootAttackVerificationProcess',
                        args=[networkIf, self.__partialResults, Utils.getSniffedStpPacket(networkIf)]))

        # start the attack process
        attackProcess.start()

        time.sleep(4)

        # start the verification processes consecutively
        for i in range(numberOfVerifProcesses):
            verificationProcesses[i].start()
            verificationProcesses[i].join()
            time.sleep(2)

        # stop the attack process
        attackProcess.terminate()
        time.sleep(2)

    def startAttack(self, attackGlobalParameters):
        pyersinia_lib.api.run(attackGlobalParameters)

    def startAttackVerification(self, networkIf, resultList, stpPacket):
        #sniff a STP packet
        newStpPacket = Utils.getSniffedStpPacket(networkIf=networkIf)

        # compute expected MAC address of new Root bridge
        # Author of the source code below for new Root bridge MAC calculation:
        # Nottingham Prisa Team.
        # URL: https://github.com/nottinghamprisateam/pyersinia/blob/master/pyersinia_lib/libs/plugins/stp_root_role.py
        rootMAC = stpPacket.rootmac
        rootMAC = rootMAC[::-1]
        newMAC = ''
        aux = False

        for x in range(len(rootMAC)):
            if (rootMAC[x] in '123456789abcdefABCDEF') and not aux:
                n = int(rootMAC[x], 16)
                n -= 1
                n = format(n, 'x')
                newMAC += n
                aux = True
            else:
                newMAC += rootMAC[x]
        expectedNewRootMac = newMAC[::-1]

        # compare Root bridge MAC addresses
        if newStpPacket.rootmac == expectedNewRootMac:
            resultList.append(True)
        else:
            resultList.append(False)


    def testPassed(self):
        if globals.debugMode:
            print('[DEBUG] Partial results: ', self.__partialResults)

        if self.__partialResults.__contains__(False):
            return True
        else:
            return False

    def getTestName(self):
        return self.__name

    def getTestManual(self):
        return self.__testManual