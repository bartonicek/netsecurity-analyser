import time
import iptc
import netifaces
import ipaddress
import pyersinia_lib.api

from multiprocessing import Process, Manager
from app import Utils, globals


class DhcpSnoopingTest:
    # base variables declaration
    manager = Manager()
    __name = 'DHCP snooping test'
    __testManual = 'Please connect both network interfaces to switch access ports for endpoint devices (e.g. computer) in the same VLAN. DHCP server presence required'
    __partialResults = manager.list()

    def __init__(self):
        pass

    def start(self, networkIf, attackingIf):
        # attack variable declaration
        rogueDhcpServerNetworkCIDR = "5.5.5.0/24"
        attackingIfIp = netifaces.ifaddresses(attackingIf)[netifaces.AF_INET][0]['addr']

        # start attack and verification processes
        processRogueDhcpServer = Process(target=self.startDhcpRogueServer, name='rogueDhcpServerThread',
                                         args=(networkIf, attackingIf, rogueDhcpServerNetworkCIDR))
        processAttackVerification = Process(target=self.startAttackVerification, name='attackVerificationThread',
                                            args=(networkIf, rogueDhcpServerNetworkCIDR, self.__partialResults))

        # create and insert iptables rule for block DHCP offers
        fwDhcpBlockRule = self.getAuxiliaryFirewallRule(networkIf, attackingIfIp)
        self.insertAuxiliaryFirewallRule(fwDhcpBlockRule)

        # start the rogue DHCP server
        processRogueDhcpServer.start()

        time.sleep(5)

        # start the attack verification
        processAttackVerification.start()
        processAttackVerification.join()

        time.sleep(3)

        # do some cleanup...
        print('[INFO] Stopping rogue DHCP server...')
        processRogueDhcpServer.terminate()
        self.removeAuxiliaryFirewallRule(fwDhcpBlockRule)

    def startDhcpRogueServer(self, primaryNetworkIf, attackingIf, rogueDhcpServerNetworkCIDR):
        try:
            gatewayIp = netifaces.gateways()['default'][netifaces.AF_INET][0]
            netmask = netifaces.ifaddresses(primaryNetworkIf)[netifaces.AF_INET][0]['netmask']
        except:
            print('[ERROR] Error during determine the gateway IP')

        __testPingIp = gatewayIp

        # declare some parameters for DHCP rogue server
        attackingIfIp = netifaces.ifaddresses(attackingIf)[netifaces.AF_INET][0]['addr']
        rogueDhcpNetwork = ipaddress.ip_network(rogueDhcpServerNetworkCIDR, strict=False)

        attackConfig = pyersinia_lib.api.GlobalParameters()
        attackConfig.attack = ['dhcp_rogue']
        attackConfig.interface = [attackingIf]
        attackConfig.ipserver = str(attackingIfIp)
        attackConfig.gateway = str(gatewayIp)
        attackConfig.network = str(rogueDhcpNetwork.network_address)
        attackConfig.netmask = str(rogueDhcpNetwork.netmask)
        attackConfig.domain = 'netSecurityTest.domain'
        attackConfig.server_domain = str(attackingIfIp)

        if globals.debugMode:
            print("[DEBUG] DHCP rogue server config:")
            print("[DEBUG] DHCP network:", str(rogueDhcpNetwork.network_address))
            print("[DEBUG] DHCP netmask:", str(rogueDhcpNetwork.netmask))
            print("[DEBUG] DHCP name server:", str(attackingIfIp))

        # start the DHCP rogue server
        pyersinia_lib.api.run(attackConfig)

    def startAttackVerification(self, primaryNetworkIf, rogueDhcpServerNetworkCIDR, resultList):
        # obtain new IP on verification interface with falsed MAC addr
        Utils.bringNetworkIfDown(primaryNetworkIf)
        time.sleep(2)
        Utils.falseNetworkIfMacAddr(primaryNetworkIf)
        time.sleep(2)
        Utils.bringNetworkIfUp(primaryNetworkIf)
        time.sleep(2)
        Utils.renewIpFromDhcp(primaryNetworkIf)
        time.sleep(4)

        # try to determine if current IP address on the verification interface belongs to the rogue DHCP server network
        try:
            interfaceIpAsNetwork = ipaddress.ip_network(
                (netifaces.ifaddresses(primaryNetworkIf)[netifaces.AF_INET][0]['addr']), strict=False).network_address

            if globals.debugMode:
                print('[DEBUG] Verificated interface IP:', str(interfaceIpAsNetwork))

            if interfaceIpAsNetwork in list(ipaddress.ip_network(rogueDhcpServerNetworkCIDR).hosts()):
                resultList.append(True)
                if globals.debugMode:
                    print("[DEBUG] IP addr has been obtained by rogue DHCP server!")
            else:
                resultList.append(False)
                if globals.debugMode:
                    print("[DEBUG] IP addr has not been obtained by rogue DHCP server!")
        except:
            resultList.append(False)
            if globals.debugMode:
                print("[DEBUG] IP addr has not been obtained by rogue DHCP server!")

        time.sleep(4)

    def getAuxiliaryFirewallRule(self, networkIf, allowedIp):
        if globals.debugMode:
            print('[DEBUG] FW rule rogue DHCP IP:', allowedIp)

        ruleDhcpDrop = iptc.Rule()
        ruleDhcpDrop.protocol = "udp"
        ruleDhcpDrop.in_interface = networkIf
        ruleDhcpDrop.src = ("!" + str(allowedIp))
        ruleDhcpDrop.target = iptc.Target(ruleDhcpDrop, "DROP")

        return ruleDhcpDrop

    def insertAuxiliaryFirewallRule(self, rule):
        # create new firewall rule which will block DHCP offers
        match = iptc.Match(rule, "udp")
        match.sport = "67"
        match.dport = "68"
        chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "INPUT")
        rule.add_match(match)
        chain.insert_rule(rule)

        if globals.debugMode:
            print('[DEBUG] Aux FW rule for DHCP blocking inserted')

    def removeAuxiliaryFirewallRule(self, rule):
        chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "INPUT")
        chain.delete_rule(rule)

        if globals.debugMode:
            print('[DEBUG] Aux FW rule for DHCP blocking removed')

    def testPassed(self):
        if globals.debugMode:
            print('[DEBUG] Partial results: ', self.__partialResults)

        if self.__partialResults.__contains__(False):
            return True
        else:
            return False

    def getTestName(self):
        return self.__name

    def getTestManual(self):
        return self.__testManual