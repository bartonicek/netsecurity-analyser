# NetSecurity Analyser
It is practical part (Python application) of my diploma thesis.

**Annotation**

Diploma thesis describes functionality of L2 networks, their protocols, known attacks which occur in this type of networks and ways of defense against these attacks.
Theoretical part contains information about most used L2 protocols – Ethernet, ARP, 802.1Q, STP and CRP, continues with description of known network attacks such as ARP spoofing, ARP cache poisoning, MAC spoofing, CAM table overflow, DHCP starvation, VLAN hopping, STP root bridge change. Thereafter technologies which prevents using of these attacks in computer network are described (Dynamic ARP inspection, Port-security, DHCP snooping, STP Root Guard, STP BPDU Guard).
In practical part, author creates tool which is able to test network security comprehensively after start them on a computer connected to network. The aim is development of application enabling penetration testing of networks against L2 protocol attacks and test their functionality on network model with real network devices.

**Author**

Bc. Tomas Bartonicek

**Organization**

Faculty of informatics and management, University of Hradec Kralove
