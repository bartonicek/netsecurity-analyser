from os.path import  dirname, join
import setuptools

with open(join(dirname(__file__), 'requirements.txt')) as f:
    required = f.read().splitlines()

setuptools.setup(
    name="netSecurityAnalyser",
    version="1.0.0",
    author="Bc. Tomas Bartonicek",
    author_email="tomas.bartonicek@uhk.cz",
    description="It is a Python tools which include pack of test security features presence in L2 network.",
    url="https://gitlab.com/bartonicek/netSecurity-Analyser",
    packages=setuptools.find_packages(),
    install_requires=required,
    include_package_data=True,
    entry_points = {'console_scripts': [
        'netsecurityanalyser = app.Start:main'
    ]},
    dependency_links=[
        'git+https://github.com/tomasbartonicek/pyersinia.git@master#egg=pyersinia'
    ]
)
